<?php
/**
* Page Template - This template is used as the default template of the page
*
*/
get_header(); ?>
<div class="container container-page">
  <div class="row">
    <div id="content" class="main-content-inner col-lg-12">
      <?php
      // Start the loop.
      while ( have_posts() ) : the_post();
      // Include the page content template.

      ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
          <h1 class="page-title"><?php the_title(); ?></h1>
        </header><!-- .entry-header -->

        <div class="entry-content">

          <div class="row">
            <?php if (has_post_thumbnail()): ?>
              <div class="col-lg-4">
                <div class="entry-content-thumbnail">
                  <?php the_post_thumbnail('actu', array('class' => 'img-fluid')); ?>
                </div>
              </div>
              <div class="col-lg-8">
                <p class="actualites-date">
                  <span><?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y'); ?></span>
                </p>
                <?php the_content(); ?>
              </div>
            <?php else: ?>
              <div class="col-lg-12">
                <p class="actualites-date">
                  <span><?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y'); ?></span>
                </p>
                <?php the_content(); ?>
              </div>
            <?php endif; ?>

          </div>
        </div><!-- .entry-content -->
        <footer class="entry-meta">
          <?php edit_post_link( __( 'Edit', 'wp-bootstrap-starter' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
        </footer><!-- .entry-meta -->
      </article><!-- #post-## -->
    <?php endwhile; ?>
  </div>
</div>
</div>
<?php get_footer(); ?>
