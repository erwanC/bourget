<?php
/*
Template Name: Archives
*/
get_header();

?>

<div id="page-header">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <?php if ( ! empty ( $sous_titre ) ) { ?>
        <p class="sous-titre"><?php echo $sous_titre; ?></p>
        <?php } ?>
    </div>
</div>

<section id="content">
  <div class="container archive">
    <div class="row">
      <?php $archive_query = new WP_Query('showposts=1000'); while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
        <div class="col-lg-12 archive-item">
          <h2><?php the_title(); ?></h2>
          <p class="actualites-date">
            <span><?php the_time('d') ?>/<?php the_time('m') ?>/<?php the_time('Y'); ?></span>
          </p>
          <?php the_excerpt(); ?>

            <a class="bouton" href="<?php the_permalink() ?>" rel="bookmark" title="Lien permanent vers <?php the_title(); ?>">En savoir+</a>
          </div>
        <?php endwhile; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
