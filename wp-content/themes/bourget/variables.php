<?php
$zoan_variables = array(
  'name' => 'Zoan',
  'phone' => '02 40 28 80 94',
	'street' => '11 place de l\'Église',
	'city' => 'Lusanger',
	'zip' => '44590',
  'legalNotice' => 'SIRET : 50059436100023 | APE : 7022Z | SARL au capital de 15 000 €'
);

$cms_variables = array(
	//1 pour ovh 2 pour phpnet
	'hebergement' => 1,
	'headerLogo' => 'logo-bourget.png',
  'name' => 'Bourget traiteur',
  'name2' => 'Maison Bourget',
  'tagline' => 'Traiteur sur Nantes depuis 1978',
  'phone' => '02 40 74 27 84',
	'street' => '101 bd Dalby',
	'city' => 'Nantes',
  'region' => 'Pays de la Loire',
	'zip' => '44000',
  'legalNotice' => 'SIRET : 47804281500017 | APE : 5621Z | SARL au capital de 10 000 €',
  'socialNetwork' => array(
    'linkedin' => '',
    'facebook' => 'https://www.facebook.com/bourgettraiteur/',
    'youtube' => '',
    'twitter' => '',
    'instagram' => 'https://www.instagram.com/bourgettraiteur/?hl=fr',
  ),
  'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2709.7669052099122!2d-1.5334278841726594!3d47.22114302251799!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4805eec2fd113099%3A0xcc8d8f58a49bb99e!2sBourget%20Traiteur!5e0!3m2!1sfr!2sfr!4v1568984489501!5m2!1sfr!2sfr',
  'schedule' => array(
    'lun. : 07:00 – 18:00',
    'mar. : 07:00 – 18:00',
    'mer. : 07:00 – 18:00',
    'jeu. : 07:00 – 18:00',
    'ven. : 07:00 – 18:00',
    'sam. : 07:00 – 18:00',
    'dim. : 09:00 – 13:00',
  ),
);

//footer
$footer_variables = array(
	'navLocations' => array(
		//jamais plus de 2 menus
		'navLocation1' => 'footer1',
		'navLocation2' => 'footer2',
	)
);

// page Contact
$contact_variables = array(
	'form' => '[contact-form-7 id="7" title="Formulaire de contact 1"]',
);
?>
