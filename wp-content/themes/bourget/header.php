<?php
require('variables.php');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head(); ?>

  <script type="text/javascript">
    window.addEventListener("load",function(){window.cookieconsent.initialise({theme:"edgeless",content:{message:"Ce site utilise des cookies pour vous garantir la meilleure expérience sur notre site.",dismiss:"Ok",link:"En savoir plus",href:"<?php echo get_home_url(); ?>/mentions-legales/"}})});
  </script>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body <?php body_class() ?> id="<?php if (is_front_page()) { echo "home"; } else { echo "type"; } ?>">

<?php
require('require.php'); ?>
<header class="header">
  <div class="container header-contact">
    <div class="row">

      <div class="col-lg-3 header-contact--col  d-none d-lg-block">

        <a target="_blank" href="https://goo.gl/maps/m8PMrLwjT8r2R4nW6" class="header-contact--text">
          <svg class="header-contact--icon">
            <use xlink:href="#pin"></use>
          </svg>
          <?php echo $cms_variables['city'] ?>, <?php echo $cms_variables['region'] ?>
        </a>

        <a rel="nofollow" class="header-contact--text" href="tel:<?php echo str_replace(' ','',preg_replace('{^0}','+33',$cms_variables['phone'])) ?>" class="header-contact--phone">
          <svg class="header-contact--icon">
            <use xlink:href="#phone"></use>
          </svg>
          <?php echo $cms_variables['phone'] ?>
        </a>

        <div class="socialNetwork header-contact--social">
          <a class="header-contact--link" href="<?php the_permalink(5) ?>">
            <svg class="header-contact--icon header-contact--mail">
              <use xlink:href="#mail"></use>
            </svg>
          </a>
          <?php socialNetwork('header-contact--link', 'header-contact--icon') ?>
        </div>

      </div>

      <div class="col-lg-6 header-logo">
        <a class="header-link" href="<?php echo get_home_url(); ?>">
          <img class="header-img" src="<?php echo get_template_directory_uri(); ?>/img/<?php echo $cms_variables['headerLogo'] ?>" alt="<?php echo $cms_variables['name'] ?>" />
        </a>
      </div>

      <div class="col-lg-3 header-cart d-none d-lg-block">
        <div class="header-cart--user">
          <a class="header-cart--link bouton2" rel="nofollow" href="">S'enregistrer</a>
          <a class="header-cart--link bouton" rel="nofollow" href="">Mon compte</a>
        </div>
        <div class="header-cart--blockCart">
          <a href="">
            <svg class="header-cart--icon">
              <use xlink:href="#panier"></use>
            </svg>
          </a>
          <span class="header-cart--text">Mon panier : 3 articles / 128€</span>
        </div>
      </div>

    </div>
  </div>

  <nav id="menu" class="main-navigation navbar navbar-expand-lg navbar-dark">
    <div class="container navbar-container">
      <button class="navbar-toggler nav-togglerIcon" type="button" data-toggle="collapse" data-target="#primary-menu-wrap" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div>
        <a rel="nofollow" href="" class="navbar-toggler nav-togglerIcon">
          <svg class="nav-icon">
            <use xlink:href="#key"></use>
          </svg>
        </a>
        <a rel="nofollow" href="" class="navbar-toggler nav-togglerIcon">
          <svg class="nav-icon">
            <use xlink:href="#man"></use>
          </svg>
        </a>
        <a href="" class="navbar-toggler nav-togglerIcon">
          <svg class="nav-icon">
            <use xlink:href="#panier"></use>
          </svg>
        </a>
        <a href="<?php echo get_home_url(); ?>/contact" class="navbar-toggler nav-togglerIcon">
          <svg class="nav-icon">
            <use xlink:href="#mail"></use>
          </svg>
        </a>
        <a rel="nofollow" href="tel:<?php echo str_replace(' ','',preg_replace('{^0}','+33',$cms_variables['phone'])) ?>" class="navbar-toggler nav-togglerIcon">
          <svg class="nav-icon">
            <use xlink:href="#phone"></use>
          </svg>
        </a>
      </div>
      <div class="nav d-none d-lg-block">
        <li class="<?php if (is_front_page()): ?>active<?php endif; ?>">
          <a class="nav-link nav-link--home" href="<?php echo get_home_url(); ?>">
            <svg class="nav-icon nav-icon--home">
              <use xlink:href="#maison"></use>
            </svg>
            <!-- <span class="nav-link d-block d-sm-none">Accueil</span>  -->
          </a>
        </li>
      </div>
      <?php
      wp_nav_menu( array(
        'theme_location'  => 'primary',
        'menu_id'         => 'primary-menu',
        'container'       => 'div',
        'container_class' => 'collapse navbar-collapse nav',
        'container_id'    => 'primary-menu-wrap',
        'menu_class'      => 'navbar-nav nav-list',
        'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
        // 'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 3,
        'walker'          => new wp_bootstrap_navwalker(),
      ) );
      ?>
    </div>
  </nav><!-- #site-navigation -->
</header>
  <?php if (!is_front_page()): ?>
    <div class="padding">
      <div id="breadcrumb">
        <div class="container">
          <?php
          if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('
            <ul class="breadcrumb">','</ul>
            ');
          }
          ?>
        </div>
      </div>
  <?php endif; ?>
