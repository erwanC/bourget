<?php
get_header();
require('variables.php');
?>

<section class="bandeau container-fluid">
  <div class="container bandeau-container">
    <div class="offset-xl-6 col-xl-6 col-lg-12">
      <div class="bandeau-frame">
        <h1 class="bandeau-title"><?php echo $cms_variables['name2'] ?>
          <br>
          <span class="bandeau-subtitle"><?php echo $cms_variables['tagline'] ?></span>
        </h1>
        <a class="bandeau-link" href="<?php the_permalink(5) ?>">Nous contacter</a>
      </div>
    </div>
  </div>
</section>
<main>
  <section class="prestations container-fluid">
    <div class="container">
      <div class="row">
        <h2 class="col-lg-12 prestations-title mustache">Nos prestations</h2>
        <p class="offset-lg-2 col-lg-8 prestations-text">Une cuisine traditionnelle, artisanale à base de produits frais, préparée par nos chefs cuisiniers, pâtissiers, bouchers et charcutiers.</p>
      </div>
    </div>

    <div class="container prestations-container">
      <div class="row prestations-row">
        <a href="<?php the_permalink(83) ?>" class="prestations-card prestations-card1">
          <div class="prestations-card--img prestations-card--img1"></div>
          <h3 class="prestations-card--title">Buffets</h3>
          <p class="prestations-card--text">Froids - Chauds - Debout<br>Boissons - À la Carte</p>
        </a>
        <a href="" class="prestations-card prestations-card2">
          <div class="prestations-card--img prestations-card--img2"></div>
          <h3 class="prestations-card--title">Apéritifs/Cocktails</h3>
          <p class="prestations-card--text">6 formules - À la Carte</p>
        </a>
        <a href="<?php the_permalink(16) ?>" class="prestations-card prestations-card3">
          <div class="prestations-card--img prestations-card--img3"></div>
          <h3 class="prestations-card--title">Plateaux repas</h3>
          <p class="prestations-card--text">À composer soi-même - Commande en ligne</p>
        </a>
      </div>

      <div class="row prestations-row">
        <a href="<?php the_permalink(96) ?>" class="prestations-card prestations-card4">
          <div class="prestations-card--img prestations-card--img4"></div>
          <h3 class="prestations-card--title">Mariages/Réceptions privées</h3>
          <p class="prestations-card--text">Formules - Vin d’honneur - Ateliers - Enfants</p>
        </a>

        <div>
          <div class="prestations-card--frame">
            <a href="<?php the_permalink(75) ?>" class="prestations-card prestations-card5">
              <div class="prestations-card--img prestations-card--img5"></div>
              <h3 class="prestations-card--title prestations-card--titleMin">Petits déjeuners</h3>
            </a>
            <a href="<?php the_permalink(70) ?>" class="prestations-card prestations-card6">
              <div class="prestations-card--img prestations-card--img6"></div>
              <h3 class="prestations-card--title prestations-card--titleMin">Pauses gourmandes</h3>
            </a>
          </div>
          <div>
            <a href="<?php the_permalink(102) ?>" class="prestations-card prestations-card7">
              <div class="prestations-card--img prestations-card--img7"></div>
              <h3 class="prestations-card--title">À la carte</h3>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

<?php
require('sprite_engagements.php');
?>
<section class="engagements container-fluid">
  <div class="container">
    <div class="row">
      <h2 class="engagements-title mustache">Nos engagements</h2>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-2 engagements-frame">
        <div class="engagements-img">
          <svg class="engagements-icon">
            <use xlink:href="#plat"></use>
          </svg>
        </div>
        <div class="engagements-text">Service</div>
      </div>
      <div class="col-lg-2 engagements-frame">
        <div class="engagements-img">
          <svg class="engagements-icon">
            <use xlink:href="#bulle"></use>
          </svg>
        </div>
        <div class="engagements-text">Conseil</div>
      </div>
      <div class="col-lg-2 engagements-frame">
        <div class="engagements-img">
          <svg class="engagements-icon">
            <use xlink:href="#label"></use>
          </svg>
        </div>
        <div class="engagements-text">Professionnalisme</div>
      </div>
      <div class="col-lg-2 engagements-frame">
        <div class="engagements-img">
          <svg class="engagements-icon">
            <use xlink:href="#avatar"></use>
          </svg>
        </div>
        <div class="engagements-text">Discrétion</div>
      </div>
      <div class="col-lg-2 engagements-frame">
        <div class="engagements-img">
          <svg class="engagements-icon">
            <use xlink:href="#horloge"></use>
          </svg>
        </div>
        <div class="engagements-text">Ponctualité</div>
      </div>
      <div class="col-lg-2 engagements-frame">
        <div class="engagements-img">
          <svg class="engagements-icon">
            <use xlink:href="#toc"></use>
          </svg>
        </div>
        <div class="engagements-text">Créativité</div>
      </div>
    </div>
  </div>
</section>

<aside>
  <section class="aside">
    <div class="container">
      <div class="row">
        <div class="offset-lg-1 col-lg-4">
          <div class="aside-img"></div>
        </div>
        <div class="col-lg-6 aside-block">
          <h2 class="aside-title">Manger bon, frais,<br>local & responsable</h2>
          <p class="aside-subtitle">Une promesse de chaque jour</p>
          <p class="aside-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        </div>
      </div>
    </div>

  </section>
</aside>

<section class="socialPlug d-none d-sm-block">
  <div class="container socialPlug-container">
    <div class="row">
      <div class="offset-lg-1 col-lg-5 socialPlug-block">
        <p class="socialPlug-text">Venez suivre notre actualité <br><span class="socialPlug-span">sur Facebook&nbsp;!</span></p>
      </div>
      <div class="col-lg-5 socialPlug-block">
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v4.0"></script>
        <div class="fb-page" data-href="https://www.facebook.com/bourgettraiteur/" data-tabs="timeline, messages" data-width="457" data-height="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
      </div>
    </div>
  </div>
</section>


<?php get_footer(); ?>
