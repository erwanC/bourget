<?php
require('variables.php');
?>

<?php if (!is_front_page()): ?>
</div><!--.padding-->
<?php endif; ?>
<footer class="footer container-fluid">
  <div class="container">
    <div class="row footer-row">
      <div class="col-md-12 col-lg-4">
        <img class="footer-brand--img" src="<?php echo get_template_directory_uri(); ?>/img/<?php echo $cms_variables['headerLogo'] ?>" alt="<?php echo $cms_variables['name'] ?>" />
        <p class="footer-brand--text"><?php echo $cms_variables['phone'] ?></p>
        <p class="footer-brand--text"><?php echo $cms_variables['street'] ?>&nbsp;-&nbsp;<?php echo $cms_variables['zip'] ?> <?php echo $cms_variables['city'] ?></p>
      </div>
      <div class="col-md-12 offset-lg-1 col-lg-3">
        <?php
        if (has_nav_menu($footer_variables['navLocations']['navLocation1'])) {
          wp_nav_menu( array(
            'container'         => 'false',
            'menu_class'      => 'footer-list',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location'    => $footer_variables['navLocations']['navLocation1'],
          ) );
        }
         ?>
      </div>
      <div class="col-md-12 col-lg-3">
        <ul class="footer-list">
          <li class="footer-item"><a class="footer-link" href="<?php echo get_home_url(); ?>/plan-du-site/">Plan du site</a></li>
          <li class="footer-item"><a class="footer-link" href="<?php echo get_home_url(); ?>/mentions-legales">Mentions légales</a></li>
          <li class="footer-item"><a class="footer-link" href="<?php echo get_home_url(); ?>/politique-de-confidentialite">Politique de confidentialité</a></li>
        </ul>
      </div>
    </div>
    <div id="copyright">
      <div class="container">
        © <?php echo date("Y") ?> - <?php echo $cms_variables['name'] ?> - <a target="_blank" href="https://zoan.fr/" title="Agence Zoan conseil graphique et web à Lusanger (44)">Agence Zoan conseil graphique et web</a>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
