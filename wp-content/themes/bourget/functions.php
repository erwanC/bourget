<?php


// Setup Functions

function bourget_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Nouveauté à ajouter
	add_theme_support('editor-styles');

	// Puis la même fonction qu'on utilisait auparavant pour Tiny MCE
	add_editor_style( 'editor-style.css' );

	// Éventuellement pour prendre en compte les blocs larges
	add_theme_support( 'align-wide' );

	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'    => __( 'Menu Principal'),
		'footer1'    => __( 'Menu Footer1'),
	) );
}

function gn_ajouter_styles_editeur() {
	add_editor_style( 'editor-style.css' );
	add_editor_style( 'https://fonts.googleapis.com/css?family=Coustard:400,900|Mada:300,400,500,700&display=swap" rel="stylesheet' );

}
add_action( 'init', 'gn_ajouter_styles_editeur' );

add_action( 'after_setup_theme', 'bourget_setup' );

// suppression emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

// supression barre admin en front
function wpc_show_admin_bar() {
	return false;
}
add_filter('show_admin_bar' , 'wpc_show_admin_bar');

// Chargement Scripts

function bourget_scripts() {
	wp_enqueue_style( 'bourget-style', get_stylesheet_uri() );
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), null, false);
	wp_enqueue_style( 'google_fonts', 'https://fonts.googleapis.com/css?family=Coustard:400,900|Mada:300,400,500,700&display=swap" rel="stylesheet', false );
}
add_action( 'wp_enqueue_scripts', 'bourget_scripts' );


// Bootstrap Navbar
require_once get_template_directory() . '/wp_bootstrap_navwalker.php';

// Clean Pagination

function sanitize_pagination($content) {
	// Remove role attribute
	$content = str_replace('role="navigation"', '', $content);

	// Remove h2 tag
	$content = preg_replace('#<h2.*?>(.*?)<\/h2>#si', '', $content);

	return $content;
}


// Responsive iFrame

function responsive_embed($html, $url, $attr) {
	return $html!=='' ? '<div class="embed-responsive embed-responsive-16by9">'.$html.'</div>' : '';
}


// Extrait Nombre de mots

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Images classes CSS Responsive

function add_image_responsive_class($content) {
	global $post;
	$pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
	$replacement = '<img$1class="$2 img-fluid cms-img"$3>';
	$content = preg_replace($pattern, $replacement, $content);
	return $content;
}

add_filter('the_content', 'add_image_responsive_class');




// Clean menu classes CSS

function custom_wp_nav_menu($var) {
	return is_array($var) ? array_intersect($var, array(
		//List of allowed menu classes
		'current-menu-item',
		'current-page-ancestor',
		'dropdown',
		'visible-xs',
		'hidden-sm',
		'hidden-md',
		'home',
		'company',
		'contact',
	)
	) : '';
}
add_filter('nav_menu_css_class', 'custom_wp_nav_menu');
add_filter('nav_menu_item_id', 'custom_wp_nav_menu');
add_filter('page_css_class', 'custom_wp_nav_menu');
//Replaces "current-menu-item" with "active"
function current_to_active($text){
	$replace = array(
		//List of menu item classes that should be changed to "active"
		'current-menu-item' => 'active',
		'current-page-ancestor' => 'active'

	);
	$text = str_replace(array_keys($replace), $replace, $text);
	return $text;
}
add_filter ('wp_nav_menu','current_to_active');
//Deletes empty classes and removes the sub menu class
function strip_empty_classes($menu) {
	$menu = preg_replace('/ class="sub-menu"/','/ class="dropdown-menu" /',$menu);
	return $menu;
}

add_filter ('wp_nav_menu','strip_empty_classes');


// Admin CSS

function admin_css() {

	$admin_handle = 'admin_css';
	$admin_stylesheet = get_template_directory_uri() . '/css/admin.css';

	wp_enqueue_style( $admin_handle, $admin_stylesheet );
}
add_action('admin_print_styles', 'admin_css', 11 );
add_action('login_head', 'admin_css');

add_action( 'admin_menu', 'my_remove_menu_pages' );

function my_remove_menu_pages() {

	remove_menu_page('edit-comments.php');
}

add_image_size( 'actu', 495, 195, true );


/**
*  Remove h1 from the WordPress editor.
*
*  @param   array  $init  The array of editor settings
*  @return  array         The modified edit settings
*/
function modify_editor_buttons( $init ) {
	$init['block_formats'] = 'Paragraph=p;Heading 1=h2;Heading 2=h3;Heading 3=h4;Heading 4=h5;Heading 5=h6;Preformatted=pre;';
	return $init;
}
add_filter( 'tiny_mce_before_init', 'modify_editor_buttons' );

//ajoute des classes dynamiquement au body pour qui permettent de définir la page sur laquelle on se trouve
add_filter( 'body_class', 'gkp_add_slug_body_class' );
function gkp_add_slug_body_class( $classes )
{
   if ( is_page() )
   {
	global $post;
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}


/*
* On utilise une fonction pour créer notre custom post type 'Témoignages'
*/

function wpm_custom_post_type() {

	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Témoignages', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Témoignage', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Témoignages'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Tous les témoignages'),
		'view_item'           => __( 'Voir les témoignages'),
		'add_new_item'        => __( 'Ajouter un nouveau témoignage'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer le témoignage'),
		'update_item'         => __( 'Modifier le témoignage'),
		'search_items'        => __( 'Rechercher un témoignage'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);

	// On peut définir ici d'autres options pour notre custom post type

	$args = array(
		'label'               => __( 'Témoignages'),
		'description'         => __( 'Tous les témoignages'),
		'labels'              => $labels,
		'menu_icon'						=> 'dashicons-format-quote',
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor'),
		/*
		* Différentes options supplémentaires
		*/
		'hierarchical'        => false,
		'public'              => false,
		'has_archive'         => true,
		'show_ui'         => true,
		'show_in_menu'         => true,
		'rewrite'			  => array( 'slug' => 'temoignages'),

	);

	// On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
	register_post_type( 'temoignages', $args );

}

add_action( 'init', 'wpm_custom_post_type', 0 );



// add meta box
add_action('add_meta_boxes','initialisation_metaboxes');
function initialisation_metaboxes(){
	//on utilise la fonction add_metabox() pour initialiser une metabox
	add_meta_box('id_meta_auteur', 'Auteur', 'meta_function_auteur', 'temoignages', 'normal', 'high');
}

// build meta box, and get meta
function meta_function_auteur($post){
	// on récupère la valeur actuelle pour la mettre dans le champ
	$aut = get_post_meta($post->ID,'_auteur',true);
	$date = get_post_meta($post->ID,'_date',true);
	$location = get_post_meta($post->ID,'_location',true);
	$event = get_post_meta($post->ID,'_event',true);
	echo '<label for="mon_champ">Auteur du témoignage : </label>';
	echo '<input id="mon_champ" type="text" name="mon_champ" value="'.$aut.'" />';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<label for="ma_date">Date : </label>';
	echo '<input id="ma_date" type="date" name="ma_date" value="'.$date.'" />';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<label for="mon_lieu">Lieu : </label>';
	echo '<input id="mon_lieu" type="text" name="mon_lieu" value="'.$location.'" />';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<label for="mon_evenement">Evénement : </label>';
	echo '<input id="mon_evenement" type="text" name="mon_evenement" value="'.$event.'" />';
}

// save meta box with update
add_action('save_post','save_metaboxes');
function save_metaboxes($post_ID){
 // si la metabox est définie, on sauvegarde sa valeur
 if(isset($_POST['mon_champ'])){
	 update_post_meta($post_ID,'_auteur', esc_html($_POST['mon_champ']));
 }

 if(isset($_POST['ma_date'])){
	 $datePost = $_POST['ma_date'];
	 if ($datePost) {
		 update_post_meta($post_ID,'_date', esc_html($_POST['ma_date']));
	 } else {
		 update_post_meta($post_ID,'_date', esc_html(date('Y-m-d')));
	 }
 }

 if(isset($_POST['mon_lieu'])){
	 update_post_meta($post_ID,'_location', esc_html($_POST['mon_lieu']));
 }

 if(isset($_POST['mon_evenement'])){
	 update_post_meta($post_ID,'_event', esc_html($_POST['mon_evenement']));
 }
}


 // On intercepte les soumissions de formulaire WPCF7 et on les traite pour créer un post de type "temoignages"

function function_to_hook_before_mail($contact_form) {

	if (138 == $contact_form->id()) {
		$submission = WPCF7_Submission::get_instance();
		if ($submission) {

			// On récupère les données du formulaire
			$posted_data = $submission->get_posted_data();


			// On stocke ce qu'on veut dans des variables, tout en assanissant les données
			$name = sanitize_text_field($posted_data['your-name']);
			$date = sanitize_text_field($posted_data['your-date']);
			$location = sanitize_text_field($posted_data['your-location']);
			$event = sanitize_text_field($posted_data['your-event']);


			// $subject = sanitize_text_field($posted_data['your-subject']);
			$message = implode("\n", array_map('sanitize_text_field', explode("\n", $posted_data['your-message'])));

			// On rédige le contenu de notre post ...
			$post_body = $message;

			// ... puis on insère un post de type 'temoignages' dans WordPress
			$my_post = array (
				'post_type' => 'temoignages',
				'post_title' => 'Témoignage de '.$name,
				'post_content' => $post_body,
			);

			$post_id = wp_insert_post($my_post);

			add_post_meta($post_id,'_auteur', $name, true);
			add_post_meta($post_id,'_date', $date, true);
			add_post_meta($post_id,'_location', $location, true);
			add_post_meta($post_id,'_event', $event, true);
		}
	}
}
add_action('wpcf7_before_send_mail', 'function_to_hook_before_mail');


// Bonus : on affiche une bubble avec le nombre de messages non-lus (brouillons) dans le menu d'admin

function msk_add_menu_msgform_bubble($menu) {
    $pending_count = wp_count_posts('temoignages')->draft;

    foreach($menu as $menu_key => $menu_data) {
        if ('edit.php?post_type=temoignages' != $menu_data[2])
    		continue;

        $menu[$menu_key][0] .= " <span class='update-plugins count-$pending_count'><span class='plugin-count'>" . number_format_i18n($pending_count) . '</span></span>';
    }

    return $menu;
}
add_filter( 'add_menu_classes', 'msk_add_menu_msgform_bubble');
