<?php
function socialNetwork($linkClass = "", $svgClass = "")
{
  require('variables.php');

  foreach($cms_variables as $cms_variable => $valeur){
    if ($cms_variable == "socialNetwork") {
      foreach ($valeur as $key => $link) {
        $socialNetwork = "";
        switch ($key) {
          case 'facebook':
          $socialNetwork = "#facebook";
          break;
          case 'linkedin':
          $socialNetwork = "#linkedin";
          break;
          case 'youtube':
          $socialNetwork = "#youtube";
          break;
          case 'twitter':
          $socialNetwork = "#twitter";
          case 'instagram':
          $socialNetwork = "#instagram";
          break;
        }
        if ($link != '') {
          echo '
          <a target="_blank" class="socialNetwork-link ' . $linkClass . '" href='. $link .'>
            <svg class="socialNetwork-icon ' . $svgClass . '">
              <use xlink:href=' . $socialNetwork . '></use>
            </svg>
          </a>
          ';
        }
      }
    }
  }
}
 ?>
