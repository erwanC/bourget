<?php
get_header();
/*
Template Name: Témoignages
*/
?>
<section class="temoignagesType">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="temoignagesType-title"><?php the_title() ?></h1>
        <p class="temoignagesType-text">Voici quelques messages de remerciements envoyés par les particuliers et les entreprises faisant appel à nos services.<br>
          Si vous aussi vous souhaitez nous laisser un message, n'hésitez pas à nous écrire via le formulaire ci-dessous.

          A bientôt !</p>
      </div>



      <?php
      $i = 1;
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      $loop = new WP_Query( array( 'post_type' => 'temoignages', 'posts_per_page' => 4, 'paged' => $paged, 'meta_key' => '_date', 'order' => 'desc', 'orderby' => 'meta_value meta_value_num') );
      ?>
      <?php while ( $loop->have_posts() ) : $loop->the_post();
      setlocale(LC_TIME, "fr_FR");
      $aut = get_post_meta($post->ID,'_auteur',true);
      $date = get_post_meta($post->ID,'_date',true);
      $location = get_post_meta($post->ID,'_location',true);
      $event = get_post_meta($post->ID,'_event',true);
      ?>
      <div class="col-lg-6 temoignagesType-col temoignagesType-col<?php echo $i ?>">
        <div class="temoignagesType-frame">
          <div class="temoignagesType-content">  <?php the_content() ?></div>

          <div class="temoignagesType-informations">
            <?php
            setlocale(LC_TIME, "fr_FR");
            $dateFr = strftime("%A %e %B %G", strtotime($date));
            ?>

            <p class="temoignagesType-aut"><?php echo strtolower($aut) . '<br><span>' . $dateFr ?></span></p>

            <p class="temoignagesType-location"><?php echo "(" . $location . ")" ?>&nbsp;<span class="temoignagesType-event"><?php echo $event ?></span></p>
          </div>

        </div>
      </div>
      <?php
      $i++;
    endwhile;
    $total_pages = $loop->max_num_pages;

    if ($total_pages > 1){

      $current_page = max(1, get_query_var('paged'));

      $display = "<div class='col-lg-12'>";
      $display .= "<div class='temoignagesType-pagination'>";
      $display .= paginate_links(array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => '?paged=%#%',
        'current' => $current_page,
        'total' => $total_pages,
        'prev_text'    => __('< Précédent&nbsp;'),
        'next_text'    => __('&nbsp;Suivant >'),
      ));
      $display .= "</div>";
      $display .= "</div>";

      echo $display;
    }
    ?>
  </div>
</div>

<div class="container-fluid">
   <div class="container temoignagesType-form">
     <div class="col-lg-12">
       <h2>Ecrivez-nous !</h2>
       <?php echo do_shortcode( '[contact-form-7 id="138" title="Formulaire témoignages"]' ); ?>
     </div>
   </div>
 </div>
</section>


<?php
get_footer();
