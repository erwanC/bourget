<?php
/*
Template Name: Mentions légales
*/
get_header();

require('variables.php');
?>

<section class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1><?php the_title() ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <p><strong>Site édité par <?php echo $cms_variables['name'] ?></strong><br />
      <?php echo $cms_variables['street'] ?> - <?php echo $cms_variables['zip'] ?> <?php echo $cms_variables['city'] ?><br />
      Tél.: <?php echo $cms_variables['phone'] ?><br>
      <?php echo $zoan_variables['legalNotice'] ?>
      </p>
      <p><strong>Scénarisation, conception-réalisation, production et animation du site Internet</strong><br />
      Agence <?php echo $zoan_variables['name'] ?>, <?php echo $zoan_variables['street'] ?> – <?php echo $zoan_variables['zip'] ?> <?php echo $zoan_variables['city'] ?><br />
      <a href="https://zoan.fr/">https://zoan.fr/</a></br><?php echo $zoan_variables['legalNotice'] ?></p>
      <p><strong>Hébergement du site Internet</strong><br />
      Plate-forme technique : WordPress,<br />
      <?php if ($cms_variables['hebergement'] == 1): ?>
        Hébergement : OVH 2 rue Kellermann - 59100 Roubaix - France SAS au capital de 10 069 020 € RCS Lille Métropole 424 761 419 00045 Code APE 2620Z N° TVA : FR 22 424 761 419</p>
      <?php elseif ($cms_variables['hebergement'] == 2): ?>
        Hébergement : PHPNET FRANCE 97-97 bis rue du général Mangin 38100 GRENOBLE (France) Code APE : 6202 A N° SIRET 451878128 00037 N° TVA intracommunautaire : FR32451878128</p>
      <?php endif; ?>
      <p><strong>INFORMATIONS LEGALES</strong></p>
      <p><strong>Données personnelles</strong><br />
      Nous vous rappelons que vous disposez d’un droit d’opposition, d’accès et de rectification des données qui vous concernent (art. 34 de la loi « Informatique et Libertés » n°78-17 du 16 janvier 1978). Pour l’exercer, adressez-vous au <strong><?php echo $cms_variables['phone'] ?> </strong>– <b><?php echo $cms_variables['name'] ?> </b>peut être amenée à fournir certaines informations à ses prestataires techniques afin de vous faire bénéficier de certaines fonctions du site. Cependant les dits prestataires ne sont pas autorisés à faire un quelconque usage des données pouvant vous concerner.</p>
      <p><strong>Cookies</strong><br />
      <?php echo $cms_variables['name'] ?> peut être amenée à utiliser un système de « cookies ». Un « cookie » ne nous permet pas de vous identifier. De manière générale, il enregistre des informations relatives à la navigation de votre ordinateur sur notre site (les pages que vous avez consultées, la date et l’heure de la consultation, etc.) que nous pourrons lire lors de vos visites ultérieures. En l’espèce, il contient les informations que vous venez de nous fournir. Ainsi, vous n’aurez pas besoin, lors de votre prochaine visite, de remplir à nouveau le formulaire que nous vous avons proposé. La durée de conservation de ces informations dans votre ordinateur dépend du paramétrage des options du logiciel de navigation que vous utilisez. Nous vous informons que vous pouvez en tout état de cause vous opposer à l’enregistrement de « cookies » en configurant votre logiciel de navigation selon le mode d’emploi l’accompagnant.</p>
      <p><strong>Propriété intellectuelle</strong><br />
      Tous les éléments (textes, logos, images, logiciels, mise en page, base de données, etc.) contenus dans le site et dans les sites associés sont protégés par le droit national et international de la propriété intellectuelle. Ces éléments restent la propriété exclusive de <?php echo $cms_variables['name'] ?> et/ou de ses partenaires. A ce titre, sauf autorisation préalable et écrite de <?php echo $cms_variables['name'] ?> et/ou de ses partenaires, vous ne pouvez procéder à une quelconque reproduction, représentation, adaptation, traduction et/ou transformation partielle ou intégrale, ou un transfert sur un autre site web de tout élément composant le site. Le non-respect de cette interdiction peut constituer un acte de contrefaçon sanctionné pénalement. Conformément aux dispositions du Code de propriété Intellectuelle, seule est autorisée l’utilisation des éléments composant le site à des fins strictement personnelles. Les marques et logos reproduits sur ce site sont déposés par les sociétés qui en sont propriétaires. Toute reproduction, réédition ou redistribution des noms ou logos, par quelque moyen que ce soit, sans autorisation préalable et écrite de leur titulaire concerné est interdite par la loi.</p>
      <p><strong>Responsabilité</strong><br />
      Les liens hypertextes mis en œuvre en direction des autres sites ne sauraient engager la responsabilité de <?php echo $cms_variables['name'] ?>, celle-ci n’exerçant aucun contrôle sur le contenu de ces sites. <?php echo $cms_variables['name'] ?> met tout en œuvre pour vous offrir des outils disponibles et des informations fiables. Toutefois, <?php echo $cms_variables['name'] ?> ne saurait vous garantir l’exactitude et l’actualité des informations fournies ainsi que la disponibilité de ces outils. En effet, malgré tous nos efforts, le site peut présenter des erreurs techniques et typographiques ou autres inexactitudes. En conséquence, la responsabilité de <?php echo $cms_variables['name'] ?> ne pourra être engagée du fait de l’utilisation des informations fournies et/ou des outils mis à disposition sur ce site. Les informations contenues dans le site sont non-contractuelles et sujettes à modification sans préavis.</p>
    </div>
  </div>
</section>

<?php get_footer(); ?>
