<?php
/*
Template Name: Contact
*/
get_header();
require('variables.php');
?>
<section class="container formContact">
  <div class="row">
    <div class="col-lg-12">
      <h1>Contactez <?php echo $cms_variables['name'] ?><br><?php echo $cms_variables['tagline'] ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-7">
      <p><?php echo do_shortcode($contact_variables['form']); ?></p>
    </div>
    <div class="col-lg-5 formContact-box">
      <div class="formContact-box text-center">
        <h2 class="formContact-box--title">Coordonnées</h2>
        <p class="formContact-box--text"><?php echo $cms_variables['street'] ?><br />
          <?php echo $cms_variables['zip'] ?> <?php echo $cms_variables['city'] ?><br />
          Tél. : <?php echo $cms_variables['phone'] ?>
        </p>

        <?php
        if ($cms_variables['schedule']) {
          echo '<h2 class="formContact-box--title">Horaires</h2>';
        }
        ?>

        <p class="formContact-box--text">
          <?php
          foreach($cms_variables as $cms_variable => $valeur){
            if ($cms_variable == "schedule") {
              foreach ($valeur as $key => $hour) {
                if ($hour != '') {
                  echo $hour . '<br>';
                }
              }
            }
          }
          ?>
        </p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <iframe class="formContact-box--map" style="border: 0;" src="<?php echo $cms_variables['map'] ?>" width="1170" height="300" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
    </div>
  </div>
</section>
<?php get_footer(); ?>
