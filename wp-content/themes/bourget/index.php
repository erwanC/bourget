<?php
get_header();

//  $sous_titre = get_post_meta($post->ID, 'sous-titre', true);

?>

<div id="page-header">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <!-- <?php if ( ! empty ( $sous_titre ) ) { ?>
        <p class="sous-titre"><?php echo $sous_titre; ?></p>
        <?php } ?> -->
    </div>
</div>

<section id="content">
	<div class="container">
        <?php while(have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
	</div>
</section>

<?php get_footer(); ?>
