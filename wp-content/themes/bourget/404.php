<?php
/**
* The template for displaying 404 pages (not found)
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package zoan
*/

get_header();
?>

<div id="primary" class="content-area container type-content">
	<main id="main" class="site-main row">

		<section class="error-404 not-found col-lg-12">
			<header class="page-header">
				<h1 class="page-title">Oups ! Cette page n'a pas été trouvée</h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<p class="text-center">On dirait que rien n'a été trouvé à cet endroit. Peut-être effectuer une recherche ou essayer l'un des liens ci-dessous ?</p>

				<div class="d-flex justify-content-center">
					<?php get_search_form(); ?>
				</div>

			<div class="row mt-5">
				<div class="offset-md-2 col-lg-3">
					<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
				</div>

				<div class="offset-md-2 col-lg-3">
					<h2><?php _e('Pages', 'textdomain'); ?></h2>
					<ul><?php wp_list_pages("title_li=" ); ?></ul>
				</div>
			</div>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
