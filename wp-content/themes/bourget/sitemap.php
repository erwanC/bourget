<?php
/*
Template Name: Plan du site
*/
 ?>
<?php get_header(); ?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h1>Plan du site</h1>
    </div>
  </div>
  <div class="row">
    <div id="content" class="offset-lg-2 col-lg-6">
      <h2><?php _e('Pages', 'textdomain'); ?></h2>
      <ul><?php wp_list_pages("title_li=" ); ?></ul>

    </div>
  </div>
</div>
<?php get_footer(); ?>
