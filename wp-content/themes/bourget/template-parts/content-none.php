<section class="no-results not-found container">
	<div class="row">
    <header class="page-header col-lg-12">
  		<h1 class="page-title text-center"><?php _e( 'Rien n\'a été trouvé', 'bourget' ); ?></h1>
  	</header><!-- .page-header -->

  	<div class="page-content col-lg-12 text-center">
  		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

  			<p><?php printf( __( 'Prêt à publier un article ? <a href="%1$s">Commencez ici</a>.', 'bourget' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

  		<?php elseif ( is_search() ) : ?>

  			<p><?php _e( 'Désolé, mais rien ne correspond à vos termes de recherche. Veuillez réessayer avec d\'autres mots-clés.', 'bourget' ); ?></p>

  		<?php else : ?>

  			<p><?php _e( 'Il semble que nous ne puissions pas trouver ce que vous cherchez. Peut-être que la recherche peut aider.', 'bourget' ); ?></p>

  		<?php endif; ?>
  	</div><!-- .page-content -->
  </div>
</section><!-- .no-results -->
