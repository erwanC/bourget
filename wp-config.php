<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bourget' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!pX=B&!CLSJ:Fjz^HkHajiGoA]Aa}oE)P&~i:_i=`J.G-urCwPe3)Pg1j)4BxlO$' );
define( 'SECURE_AUTH_KEY',  's0g6uc!ks3hDy_l-MSSlH7:27,=$f`E;QQJ^- P@=I3rH6:qim9$xjqR(I+1}i5m' );
define( 'LOGGED_IN_KEY',    'mBMXZ0safw<j,%m}6b=@@][2_@](pY/wX6G@NynY)Qo-pRAxY%M+Ht?D)u;:<({i' );
define( 'NONCE_KEY',        '-B6^N33Y,[KmItlk5XJJZZu(Jd[~j~3{2~SAHPf)&b700{|n[bC?`vLRrt,%L)hJ' );
define( 'AUTH_SALT',        'pA.LU2ts1^w,9q:f&KFvcuqx#c::Bw%fLRJ$c2F4{k8P&pVSU)Sw9gY(^x-L5?p2' );
define( 'SECURE_AUTH_SALT', 'mjw//;4o<qw]sv}oa& HdXSW7:cj7o[8&_)E@^l# *NA+/dMI91{Q7r5HHRG,zG8' );
define( 'LOGGED_IN_SALT',   'Gt`^n_bW%c^>OIx_%u56q@}gM61$3pz}l@}s`V,9;>vUeuT}cPC[`m~ayL]^u[{~' );
define( 'NONCE_SALT',       '&mZ?p8y;3-:m!J>V,B@z*or<&[QzGrq+]=v1N2Yu)t)ffj8&|wRm1A7},@T7dS_)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'kjbsdkuhxdv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
